# Vaga de desenvolvedor full stack

##### Olá candidato desenvolvedor, tudo bem?
##### Veja a seguir os passos para realizar o teste para a vaga de full stack developer na GUEP.
##### Desejamos a você uma boa sorte!

# Descrição

##### Para realizar o teste de desenvolvedor full stack é necessário ter a experiência em desenvolvimento API Rest utilizando Laravel 6+, banco de dados Mysql e o framework frontend Angular 9+.

### Neste teste é necessário que você desenvolva um CRUD de empresas com os seguintes campos:

* Razão social
* Nome fantasia
* CNPJ

### Além disso, é necessário fazer um CRUD do responsável dessa empresa com os seguintes campos:

* Nome
* CPF
* Email

# Diferenciais

* Fazer testes na aplicação utilizando TDD
* Fazer documentação da API utilizando Swagger
* Fazer documentação de como executamos a aplicação
* Desenvolver fluxogramas
* Colocar as aplicações em docker






